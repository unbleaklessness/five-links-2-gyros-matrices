robot = fivelinks_2gyros_VC_MJX;

robot.l = [ 1; 1; 1; 1; 1 ];
robot.m = [ 25; 5; 5; 5; 5 ];
robot.r = 0.02;
robot = robot.J_calcs();
robot = robot.COM_calcs();
robot.qa = zeros(5, 2);

tic;
M = M_matrix(robot.J, robot.m, robot.c, robot.l, robot.qa);
toc;
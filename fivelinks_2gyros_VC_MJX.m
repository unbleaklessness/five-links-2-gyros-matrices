classdef fivelinks_2gyros_VC_MJX   %<   handle   % ( only one instance of this class can exist )
    %FIVELINKS_VC_MJX 5-links robot for MuJoCo with HZD walking controller
        
    properties
        g = 9.82, ...%
        % lengths of links 
        l, ... = [ 1 ; 1 ; 1 ; 1 ; 1 ] ; %
        m, ... = [ 25 ; 5 ; 5 ; 5 ; 5 ]; %
        r,... = 0.02; %     
        c, %
        J, %        
        
        gyro_vel, % gyro rotation velocity
        gyro_act_int = 0, %
        Kgyro, %
        PID, %
        
        leg_stay, % 
        leg_stay_naive, %
        t_step, %
        
        Ks, %
        Kv, %
        Ktvc, %
        
        q, % state vector
        u, % control vector
        qa, %
        q0, %
        t_final,
        curr_t, %      
        model, %
        
        ctrl, 
        kk, %
%         q_plus, %
%         q_minus, %
%         alpha, %
        theta = 0, %
        dtheta = 0, %
        theta_max, %
        
        thetadesdata, %
        
        motion_const_data, %
       
        steps_count,% ??????? ?????
       
        RESHUF = [1, 4, 5, 2, 3] % new order of coordinates in case of reshuffling
        
    end
    
    methods
       
        function obj = J_calcs(obj)
            % J_CALCS calculates vector of inertia moments
            obj.J = zeros(size(obj.l,1),1);
            for ii = 1 : size(obj.l,1)
                obj.J(ii) = obj.m(ii)*(obj.l(ii)^2)*1/12 + obj.m(ii)*(obj.r^2)/4;   
            end
        end
         
        function obj = COM_calcs(obj)
            % COM_CALCS calculates center of mass coordinates for each link
            obj. c = zeros(size(obj.l,1),1);
            for ii = 1 : size(obj.l,1)
                obj. c(ii) = obj.l(ii)/2;   
            end
        end
        
        function obj = start(obj)
            % START starts MJX simulation of "MODEL" model
            mjx('load', obj.model);    
            Q = obj.vec2struct();

            mjx('set', 'qpos', [Q.x  0  Q.z  angle2quat(0, Q.q1, 0)  Q.g1  Q.g2  -Q.g1  -Q.g2  Q.q2  Q.q3  Q.q4  Q.q5]);
            mjx('set', 'qvel', [ Q.x_d 0 Q.z_d  0  Q.q1_d  0  Q.g1_d  Q.g2_d  -Q.g1_d  -Q.g2_d  Q.q2_d  Q.q3_d  Q.q4_d  Q.q5_d]);
            mjx('forward');
        end  
        
        function obj = step(obj)
            %STEP forces MJX simulation to calculate and visualize next step 
            
             obj = obj. getState();
             temp = obj.theta;
             obj = obj. get_qa();
             obj = obj. reshuffle_qa();  
             c = [1, 1, 0.5, 0, 0];   % these values are the choice vectors for the state variable 
             
             obj.theta = c*obj.qa(1:5,1); % current mechanical phase
             obj.dtheta = c*obj.qa(1:5,2); % current mechanical phase speed
             
             obj = obj. VC_PD_control();
%              obj = obj. VC_transversal_control();
             obj = obj. reshuffle_u();             
%              if mjx('time') > 5
%                     disp(obj. u)
%              end

             obj = obj.ctrlSwitch(temp);            
             
%              obj = obj. gyrosLQR();
             obj = obj. gyrosPID();
        
             % pushing the control forces to the simulation model
             limit = 2000; % saturation of actuation forces/torques             
%               disp('^^^^^^^^^^^^^^^^')
%              disp(obj.u(2:5)');
%              disp('-------------')
%              obj.u(2:5) = min(limit , max(-limit , obj. u(2:5))); 
%              disp(obj.u(2:5)');
%              disp('^^^^^^^^^^^^^^^^')
             
             mjx('set','ctrl', [0, 0, 0, 0, (obj. u(2:5))']);
             % setting CMG actuation control (velocity and acceleration) 
%              mjx('set','qacc', ((obj.q(9,2)*[ 0 0 0 0 0 0 1 0 -1 0 0 0 0 0 ] - mjx('get','qvel').*[ 0 0 0 0 0 0 1 0 1 0 0 0 0 0 ])/obj.t_step + mjx('get','qacc').*[ 1 1 1 1 1 1 0 0 0 0 1 1 1 1 ] )); 
%              mjx('set','qvel', (mjx('get','qvel').*[ 1 1 1 1 1 1 0 0 0 0 1 1 1 1 ] + obj.gyro_vel*[ 0 0 0 0 0 0 0 1 0 -1 0 0 0 0 ] + obj.q(9,2)*[ 0 0 0 0 0 0 1 0 -1 0 0 0 0 0 ]));
%               mjx('set','qvel', (mjx('get','qvel').*[ 1 1 1 1 1 1 1 0 1 0 1 1 1 1 ] + obj.gyro_vel*[ 0 0 0 0 0 0 0 1 0 -1 0 0 0 0 ]));... + obj.q(9,2)*[ 0 0 0 0 0 0 1 0 -1 0 0 0 0 0 ]));
    
         
              mjx('set','qpos', mjx('get','qpos').*[ 1 1 1 1 1 1 1 0 1 0 1 1 1 1 1 ]);
         
         
             disp(obj. u(2:5));
            % producing dynamics simulation
             mjx('step');               
        end       
        
        function  Qs = vec2struct(obj)
            % VEC2STRUCT converts state matrix Q into structure QS
            % coordinates convertion
            Qs. x = obj.q(6, 1);
            Qs. z = obj.q(7, 1);
            Qs. q1 = obj.q(1, 1);
            Qs. q2 = obj.q(2, 1);
            Qs. q3 = obj.q(3, 1);
            Qs. q4 = obj.q(4, 1);
            Qs. q5 = obj.q(5, 1);
            
            Qs. g1 = obj.q(9, 1);
            Qs. g2 = obj.q(10, 1);
            
            % velocities convertion
            Qs. x_d = obj.q(6, 2);
            Qs. z_d = obj.q(7, 2);
            Qs. q1_d = obj.q(1, 2);
            Qs. q2_d = obj.q(2, 2);
            Qs. q3_d = obj.q(3, 2);
            Qs. q4_d = obj.q(4, 2);
            Qs. q5_d = obj.q(5, 2);  
            Qs. g1_d = obj.q(9, 2);
            Qs. g2_d = obj.q(10, 2);
        end  
        
    
        function obj = getState(obj)
            % GETSTATE updates information about current state of multi-link
            % object
            
            temp = mjx('get','qpos');
            temp(4 : 7) = [quat2eul(temp(4 : 7),'ZYX'), -5]; % converting quaternion of body to euler angles 
%             temp(4 : 7) =  [quat2eul(temp(4 : 7),'XYZ'), -5]; % converting quaternion of body to euler angles 
 
            % q1 q2 q3 q4 q5 x z qx g1 g2
            obj. q(1 : 10, 1) = [temp(5) ; temp(12) ; temp(13) ; temp(14) ; temp(15) ; temp(1) ; temp(3) ; temp(6) ; temp(8) ; temp(9)];
%             obj. q(1 : 10, 1) = [temp(5) ; temp(12) ; temp(13) ; temp(14) ; temp(15) ; temp(1) ; temp(3) ; temp(4) ; temp(8) ; temp(9)];

            temp = mjx('get','qvel');
            % q1_d q2_d q3_d q4_d q5_d x_d z_d qx_d g1_d g2_d
            obj. q(1 : 10, 2) = [temp(5) ; temp(11) ; temp(12) ; temp(13) ; temp(14) ; temp(1) ; temp(3) ; temp(4) ; temp(7) ; temp(8)];  
                        
            temp = mjx('get','sensordata');
            obj. leg_stay =  [temp(5) temp(6)] ~= 0; % touch detection
%             disp(obj. leg_stay);
        end
        
        function obj = get_qa(obj)
            % GET_QA calculates transformation from MuJoCo coordinates to FROST coordinates          
           
            obj. qa(1) = obj.q(1);
            obj. qa(2) = pi + obj.q(2);
            obj. qa(3) = obj.q(3);
            obj. qa(4) = pi + obj.q(4);
            obj. qa(5) = obj.q(5);
            obj. qa(1,2) = obj.q(1,2);
            obj. qa(2,2) = obj.q(2,2);
            obj. qa(3,2) = obj.q(3,2);
            obj. qa(4,2) = obj.q(4,2);
            obj. qa(5,2) = obj.q(5,2);
%             disp('obj.qa');
%             disp(obj.qa);
%             pause;
        end   
        
        function obj = reshuffle_qa(obj)
            %GET_QA calculates transformation from MuJoCo coordinates to FROST coordinates          
                       
            % finding x-coordinates of both legs end-effectors
            xw1 = obj.q(6) - obj.l(2)*sin(obj.q(2)) - obj.l(3)*sin(obj.q(2)+obj.q(3));
            xw2 = obj.q(6) - obj.l(4)*sin(obj.q(4)) - obj.l(5)*sin(obj.q(4)+obj.q(5));            
                
            % ignoring the flight phase
            if sum(obj.leg_stay) ~= 0                
                    obj.leg_stay_naive = obj.leg_stay;                    
            end          

         %% reshuffling procedure based on the rule that front leg is
            % always the support leg
            switch obj.leg_stay_naive(1)
                case 0
                    switch obj.leg_stay_naive(2)
                        case 0
                            % never executes
                        case 1 
                           obj. qa(:, 1) = obj. qa(obj.RESHUF, 1);
                           obj. qa(:, 2) = obj. qa(obj.RESHUF, 2);
%                            disp('reshuffle');
                    end
                case 1
                    switch obj.leg_stay_naive(2)
                        case 0
                            %
                        case 1
                            if xw1 < xw2  % the right leg is front
                               obj. qa(:, 1) = obj. qa(obj.RESHUF, 1);
                               obj. qa(:, 2) = obj. qa(obj.RESHUF, 2);
%                                disp('reshuffle');
                            end
                    end
            end                
            
        end           
        
        
        function [M, Cg] = alt_matr(obj)
            %ALT_MATR calculates inertia matrix M(q) and gravitational-Coriolis matrix Cg(q, q_dot) for 5-links
            %robot
            
            M = M_matrix_mex(obj.J, obj.m, obj.c, obj.l, obj.qa);

            Cg = Cg_matrix(obj.g, obj.c, obj.m, obj.l, obj.qa);
        end
        
        function [ h, h_q, h_qq ] = VC_generation(obj)
            %VC_GENERATION calcuates virtual constrains based on
            %pre-computed polynomial coefficients
          
            c = [1, 1, 0.5, 0, 0];   % these values are the choice vectors for the state variable 
           
            M = 5; % number of DoF
            theta_plus = c*obj.ctrl(obj.kk).q_plus; % mechanical phase after interaction
            theta_minus = c*obj.ctrl(obj.kk).q_minus;            

            s = (obj.theta - theta_plus)/(theta_minus - theta_plus); % relational mechanical phase

        %% calculation of Bezier coefficients
            obj.ctrl(obj.kk).alpha_d = zeros(4,M);            
         
            for i = 1:M
                obj.ctrl(obj.kk).alpha_d(:,i) = obj.ctrl(obj.kk).alpha(:,i+1) - obj.ctrl(obj.kk).alpha(:,i);
            end

            obj.ctrl(obj.kk).alpha_dd = zeros(4,M-1);
            
            for i = 1:M-1
                obj.ctrl(obj.kk).alpha_dd(:,i) = obj.ctrl(obj.kk).alpha(:,i+2) - 2*obj.ctrl(obj.kk).alpha(:,i+1) + obj.ctrl(obj.kk).alpha(:,i);
            end

            b = zeros(M+1,1);
            
            for k = 0:M
                b(k+1) = (factorial(M)/(factorial(k)*factorial(M-k)))*s^k*(1-s)^(M-k);
            end

            b_d = zeros(M,1);
            
            for k = 0:M-1
                b_d(k+1) = (factorial(M)/(factorial(k)*factorial(M-k-1)))*s^k*(1-s)^(M-k-1);
            end

            b_dd = zeros(M-1,1);
            
            for k = 0:M-2
                b_dd(k+1) = (factorial(M)/(factorial(k)*factorial(M-k-2)))*s^k*(1-s)^(M-k-2);
            end
            
        %% final Bezier coefficients
            h = obj.ctrl(obj.kk).alpha*b;
            h_q = obj.ctrl(obj.kk).alpha_d*b_d;
            h_qq = obj.ctrl(obj.kk).alpha_dd*b_dd;           
        end    
        
        function [ obj ] = VC_transversal_control(obj)
            %VC_CONTROL computes transversal control for joints based on calculated
            %virtual constraints   
         
             % ??????????? ? ??????? ???????
            [M, Cg] = obj. alt_matr();
            [h, h_q, h_qq] =  obj. VC_generation();
            
            % ??????????, ??? ???????????????? t_phase,K_curr ? ksi(1) ? ??????, 
            % ???? ???????????? ???? ????? ?? ????? ???????????? ???????. 
            % ?????????????? ????????? ????????? ?????????.
            
            % ?????? ??????????????? ?????? ?????????: 
            % [ ???.????  , Y , Y_d ]
            ksi = zeros(9,1);
            ksi(1) = obj.dtheta^2 - obj.ctrl(obj.kk).motion_const_interp(obj.theta);
            ksi(2:5) = obj.qa(2:5,1) - h;
            ksi(6:9) = obj.qa(2:5,2) - h_q*obj.dtheta;

            % ???????? ???? ????????? ??????? ?? ??????? (????? ?? ???? ?????????? ??????? "?????" ?? ?????? ????)
            t_phase = obj.ctrl(obj.kk).theta_interp(obj.theta);
            
            % actuation matrix
            B = zeros(5,4); 
            B(2:5,:) = eye(4);

            % annihillation matrix for actuation matrix B (?????? ?????, ?????????? ?????? ??? ?????? ????????? ?? ????)
            B_annih = zeros(4,5);
            B_annih(:,2:5) = eye(4);

            % ??????? ???????? ? ?????????? (theta , Y): 
            % q = (theta Y + phi (theta)); 
            % q' = L (theta' Y'); 
            % q'' = L (theta'' Y'') + NN
            
            L = zeros(5);
            L(:,1) = [1-h_q(1)-0.5*h_q(2);h_q];
            L(1,2:3) = [-1,-0.5];
            L(2:5,2:5) = eye(4);

            N = [-h_qq(1)-0.5*h_qq(2);h_qq]*obj.dtheta^2;      
            
            % ????????? ??????? ????????????? ???????? ????? ??? ??????? ???????
            K_curr = zeros(4,9);
            for i=1:4
                for j=1:9
                    K_curr(i,j) = interp1(0:0.004:0.644,reshape(obj.Ktvc(i,j,:), [1,size(obj.Ktvc,3)]), t_phase,'spline');         
                end
            end

            
            % ????????? ???? ??????????
            obj. u(2:5,1) = (B_annih*(L\(M\B)))\(B_annih*(L\(N + M\Cg)) - K_curr*ksi);
            obj. u(1) = -obj. u(2) - obj. u(4); % resulting general force acting on the body           
            
        end
        
        function [ obj ] = VC_PD_control(obj)
            %VC_CONTROL computes PD-control for joints based on calculated
            %virtual constraints
            
             H0 = [zeros(4,1),eye(4)];
             c = [1, 1, 0.5, 0, 0];   % these values are the choice vectors for the state variable 
            
             % inertial and forces matrixes
             [M, C] = obj. alt_matr();
             [h, h_q, h_qq] =  obj. VC_generation();      

             h = H0*obj.qa(1:5, 1) - h;
             h_q = H0 - h_q*c;
             h_qq = -h_qq*c*obj. qa(1:5, 2)*c;
             
             B = zeros(5);
             B(2:5, 2:5) = eye(4);

             obj. u =([h_qq, h_q] * [obj.qa(1:5,2); M\(-C)] + obj.ctrl(obj.kk).Kv(2 : 5, 2 : 5) * h_q * obj.qa(1 : 5, 2) + obj.ctrl(obj.kk).Ks(2 : 5, 2 : 5) * h);
            
             obj. u = -(h_q * (M\ B))\obj.u;    

              
             obj. u(1) = -obj. u(2) - obj. u(4); % resulting general force acting on the body
%             disp(obj.u);

          
        end
         
        function obj = reshuffle_u(obj)
            %RESHUFFLE _U remixes the order of elements of control vector U

            % finding x-coordinates of both legs end-effectors
            xw1 = obj.q(6) - obj.l(2)*sin(obj.q(2)) - obj.l(3)*sin(obj.q(2)+obj.q(3));
            xw2 = obj.q(6) - obj.l(4)*sin(obj.q(4)) - obj.l(5)*sin(obj.q(4)+obj.q(5));            
            
         %% reshuffling procedure based on the rule that front leg is
            % always the support leg
             switch obj.leg_stay_naive(1)
                case 0
                    switch obj.leg_stay_naive(2)
                        case 0
                            % never executes
                        case 1
                        	obj. u = obj.u(obj.RESHUF); 
                    end
                case 1
                    switch obj.leg_stay_naive(2)
                        case 0
                            %
                        case 1
                            if xw1 < xw2 % the right leg is front
                            	obj. u = obj.u(obj.RESHUF); 
                            end
                    end
             end          
        end
        
        function obj = ctrlSwitch(obj,temp)
        %CTRLSWITCH switches between available trajectories based on some rule    
            
             % calculating finished steps   
             if (obj.theta < temp) && (obj.theta < pi)
                 obj. steps_count = obj.steps_count + 1;     
                 
                 % speed up/down
                 switch obj. steps_count
                     case {10, 20, 30, 50} 
%                          obj.kk = obj.kk-1;
                 end
             end            
        end
        
        function obj = gyrosLQR(obj)
            %GYROSLQR calculates velocity control using LQR of inverted
            %pendulum
             obj. gyro_act_int =  obj. gyro_act_int + obj.t_step*obj.q(9,1);
             temp = [ obj.q(8,2) , obj.q(8,1) , obj.q(9,1) , obj.gyro_act_int ];  % [ pendulum_angular_velocity  pendulum_angle  actuator_angle  actuator_angle_integrator] 
             obj. q(9,2) = temp*(obj. Kgyro)';
        end   
        
        function obj = gyrosPID(obj)
            %GYROSPID calculates velocity control using PID of inverted
            %pendulum
             obj. gyro_act_int =  obj. gyro_act_int + obj.t_step*obj.q(8,1);
             temp = [ obj.q(8,1) , obj. gyro_act_int , obj.q(8,2) ];  % [ pendulum_angle  pendulum_angle_integrator  pendulum_angular_velocity ] 
             obj. q(9,2) = temp*(obj. PID)';
        end  
        
        function obj = finish(obj)
            %FINISH forces MJX simulation to stop and closes visualization          
           
            fprintf('%d steps\n',obj.steps_count);
            tempfig = figure('Name','Press to proceed','NumberTitle','off');
            waitforbuttonpress;
            close(tempfig);        
            mjx('exit');               
        end   
               
        
%         function obj = fivelinks_VC_MJX(inputArg1,inputArg2)
%             %FIVELINKS_VC_MJX Construct an instance of this class
%             %   Detailed explanation goes here
%             obj.l = inputArg1 + inputArg2;
%         end
        
%         function outputArg = method1(obj,inputArg)
%             %METHOD1 Summary of this method goes here
%             %   Detailed explanation goes here
%             outputArg = obj.Property1 + inputArg;
%         end
    end
end

